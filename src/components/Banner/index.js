import React from 'react' 

import Botao from '../Botao'

import './styles.scss'

import HomerSimpson from '../../images/homer-simpson.svg'
import Donut from '../../images/donut.svg'

export default function Banner() {
  return (
    <div className="row-banner">
      <div className="container container-banner">
        <img className="homer-simpson" src={HomerSimpson} alt="Homer Simpson"/>
        <div className="content-buy">
          <h1>Canecas Temáticas</h1>
          <ul className="bullet-compra">
            <li className="item">Escolha o tema</li>
            <li className="item">Mande para o nosso <a href="https://api.whatsapp.com/send?phone=5511998289710">WhatsApp</a></li>
            <li className="item">Pronto! Ja iremos fazer sua caneca</li>
          </ul>
          <Botao className="cta-pedido" textoBotao="Comprar Agora" />
        </div>
        <img  className="donut" src={Donut} alt="Donut"/>
      </div>
    </div>
  )
}