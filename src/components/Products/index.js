import React from 'react' 

import Caneca from '../../images/caneca.png'

import './styles.scss'

export default function Products() {
  return (
    <>
    <div className="container container-product">
      <h2>Vários estilos para voce escolher</h2>

      <div className="products-cards">
        <div className="card">
          <span className="category">Harry potter</span>
          <img src={Caneca} alt=""/>
          <h4>Espresso Patronum</h4>
          <span className="price">R$ 35,00</span>
        </div>
        <div className="card">
          <span className="category">Harry potter</span>
          <img src={Caneca} alt=""/>
          <h4>Espresso Patronum</h4>
          <span className="price">R$ 35,00</span>
        </div>
        <div className="card">
          <span className="category">Harry potter</span>
          <img src={Caneca} alt=""/>
          <h4>Espresso Patronum</h4>
          <span className="price">R$ 35,00</span>
        </div>
        <div className="card">
          <span className="category">Harry potter</span>
          <img src={Caneca} alt=""/>
          <h4>Espresso Patronum</h4>
          <span className="price">R$ 35,00</span>
        </div>
      </div>
    </div>
    </>
  )
}