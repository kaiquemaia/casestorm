import React from 'react' 

import './styles.scss'

export default function Botao({ textoBotao, tema }) {
  return (
  <button id={tema} className="cta">{textoBotao}</button>
  )
}