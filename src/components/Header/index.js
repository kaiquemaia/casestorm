import React from 'react' 

import "./styles.scss"

import logo from '../../images/logo.svg'

export default function Header() {
  return (
    <>
    <header>
      <div className="container container-header">
        <img src={logo} alt="Logo CaseStorm"/>
        <button className="cta-pedido">Acompanhar meu pedido</button>
      </div>
    </header>
    </>
  )
}