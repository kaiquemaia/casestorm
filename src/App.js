import Header from './components/Header'
import Banner from './components/Banner'
import Products from './components/Products'

function App() {
  return (
    <>
    <Header />
    <Banner />
    <Products />
    </>
  );
}
export default App;